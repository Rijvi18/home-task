//Sum of given digit
using System;
public class ConvertDigit {
 public static void Main() {
  System.Console.Write("Input a number: ");
  int n = Convert.ToInt32(Console.ReadLine());
  int sum = 0;
  while (n != 0) {
   sum += n % 10;
   n /= 10;
  }
  Console.WriteLine("Sum of the digits: " + sum);
 }
}