
// Multiply corresponding elements of two arrays of integersusing System;
using System.Collections.Generic;

public class ArrayMul {
 public static void Main() {
     int[] first_array = {1, 3, -5, 4};
     int[] second_array = {1, 4, -5, -2};
     
      
      System.Console.WriteLine("\nMultiply corresponding elements of two arrays: ");
    
     for (int i = 0; i < first_array.Length; i++)
          {
                
          Console.Write(first_array[i] * second_array[i]+" ");
        }
     System.Console.WriteLine("\n");
   }
}