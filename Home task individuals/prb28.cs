//Reverse a string
using System;
public class ReverseStr{
    public static void Main(){ 
        System.Console.Write("Input a string : ");
        string str = Console.ReadLine();
        System.Console.WriteLine("Reverse of string: " + Reverse(str));
        string Reverse(string text)
            {
                char[] cArray = text.ToCharArray();
                string reverse = String.Empty;
                for (int i = cArray.Length - 1; i > -1; i--)
                {
                    reverse += cArray[i];
                }
                return reverse;
            }
    }

}
