// Check if a given positive number is a multiple of 3 or a multiple of 7
using System;
using System.Collections.Generic;

public class prb30 {
  static void Main(string[] args)
        {
           System.Console.WriteLine("\nInput first integer:");  
           int x = Convert.ToInt32(Console.ReadLine());
           if (x > 0)
           {
              System.Console.WriteLine(x % 3 == 0 || x % 7 == 0);
           }
        }
}