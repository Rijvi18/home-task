﻿using System;

namespace task
{
    public class Program
    {
        static void Main(string[] args)
        {
            //1.print number
            System.Console.WriteLine("Hello");
            System.Console.WriteLine("Rijvi Ahmed");

            //2.sum of two number
            int a = 10;
            int b = 30;
            int sum;
            sum = a + b;
            System.Console.WriteLine($"The sum of two number is {sum}");

            //3.Dividing of two number
            var a1 = 30.5;
            var b1 = 10.4;
            var div=0.0;
            div = a1 / b1;
            System.Console.WriteLine($"The division of two number is {div:N1}");

            //4.print the specific operation
            System.Console.WriteLine("Print the specific operations:");
            System.Console.WriteLine((-1) + (4 * 6));
            System.Console.WriteLine((35 + 5) % 7);
            System.Console.WriteLine(14 + (((-4) * 6) / 11));
            System.Console.WriteLine((2 + ((15 / 6) * 1)) - (7 % 2));

            //5.swap two numbers
            int num1, num2, temp;
            System.Console.Write("First Number:\n ");
            num1 = Convert.ToInt32(Console.ReadLine());
            System.Console.Write("Second Number:\n ");
            num2 = Convert.ToInt32(Console.ReadLine());
            temp = num1;
            num1 = num2;
            num2 = temp;
            System.Console.Write("After Swapping");
            System.Console.Write("\nFirst Number: " + num1);
            System.Console.Write("\nSecond Number: " + num2);

            //6.multiplication of three numbers
            int n1, n2, n3;

            System.Console.Write("\nfirst number: ");
            n1 = Convert.ToInt32(Console.ReadLine());

            System.Console.Write("second number: ");
            n2 = Convert.ToInt32(Console.ReadLine());

            System.Console.Write("third number: ");
            n3 = Convert.ToInt32(Console.ReadLine());

            int result = n1 * n2 * n3;
            System.Console.WriteLine($"Multiplication of three numbers: {result}\n");

            //7.print multiplication table
            int x,j;
            System.Console.WriteLine("Enter a number:");
            x = Convert.ToInt32(Console.ReadLine());
            for (j = 1; j <= 10; j++)
            {
                System.Console.Write("{0} X {1} = {2} \n", x, j, x * j);
            }


            //8.Print the average of four numbers
            double number1, number2, number3, number4;

            System.Console.Write("Enter the First number: ");
            number1 = Convert.ToDouble(Console.ReadLine());

            System.Console.Write("Enter the Second number: ");
            number2 = Convert.ToDouble(Console.ReadLine());

            System.Console.Write("Enter the third number: ");
            number3 = Convert.ToDouble(Console.ReadLine());

            System.Console.Write("Enter the fourth number: ");
            number4 = Convert.ToDouble(Console.ReadLine());

            double result2 = (number1 + number2 + number3 + number4) / 4;
            Console.WriteLine("The average of {0}, {1}, {2}, {3} is: {4} ",
            number1, number2, number3, number4, result2);

            //9.age problem
            int age;
            System.Console.Write("Enter your age ");
            age = Convert.ToInt32(Console.ReadLine());
            System.Console.Write("You look older than {0} \n", age);

            //10.Display a number in rectangle of 3 columns wide and 5 rows tall using that digit
            int t;

            Console.Write("Enter a number: ");
            t = Convert.ToInt32(Console.ReadLine());

            System.Console.WriteLine("{0}{0}{0}", t);
            System.Console.WriteLine("{0} {0}", t);
            System.Console.WriteLine("{0} {0}", t);
            System.Console.WriteLine("{0} {0}", t);
            System.Console.WriteLine("{0}{0}{0}", t);

            //11.convert from celsius degrees to Kelvin and Fahrenheit
            Console.Write("Enter the amount of celsius: ");
            int celsius = Convert.ToInt32(Console.ReadLine());

            System.Console.WriteLine("Kelvin = {0}", celsius + 273);
            System.Console.WriteLine("Fahrenheit = {0}", celsius * 18 / 10 + 32);


            //12.Remove a specific string
            String str = "w3resource";
            Console.WriteLine("Given String : " + str);
            Console.WriteLine("String1 : " + str.Remove(1, 1)); 
            Console.WriteLine("String2 : " + str.Remove(9, 1));
            Console.WriteLine("String3 : " + str.Remove(0, 1));


            //13.Create a new string from a given string where the first and last characters will change their positions
            String str1 = "w3resource";

            Console.WriteLine("String : " + str);

            // 14.replace the character 'w' with 'e' 
            Console.WriteLine("Repalce String: " + str1.Replace('w', 'e'));

            //15.Create a new string from a given string with the first character added at the front and back
            string str2;
            System.Console.Write("Input a string : ");
            str2 = Console.ReadLine();
            if (str2.Length >= 1)
            {
                var s = str2.Substring(0, 1);
                System.Console.WriteLine(s + str2 + s);
            }


            //16.Compute the sum of two given integers, if two values are equal then return the triple of their sum
            int n6, n7 ,sum1;
            System.Console.Write("\nfirst number: ");
            n6 = Convert.ToInt32(Console.ReadLine());

            System.Console.Write("second number: ");
            n7 = Convert.ToInt32(Console.ReadLine());
            if (n6 == n7)
            {
                sum1 = (n6 + n7) * 3;
            }
            else
            {
                sum1 = (n6 + n7);
            }
            System.Console.Write($"Sum: {sum1}\n ");


            //17.Get the absolute value of the difference between two given numbers
            int n9, n10;
            System.Console.Write("\nfirst number: ");
            n9 = Convert.ToInt32(Console.ReadLine());

            System.Console.Write("second number: ");
            n10 = Convert.ToInt32(Console.ReadLine());
            System.Console.WriteLine(result5(n9, n10));



            //18.Check if an given integer is within 20 of 100 or 200
            System.Console.WriteLine("\nInput an integer:");
            int x2 = Convert.ToInt32(Console.ReadLine());
            System.Console.WriteLine(result6(x2));

            //19.Print the odd numbers from 1 to 99
            int m;
            System.Console.WriteLine("Odd numbers from 1 to 99. Prints one number per line.");
            for (m = 1; m <= 99 ; m++)
            {
                if (m % 2 != 0)
                {
                    System.Console.WriteLine(m.ToString());
                }
            }

            //20.Compute the sum of the first 500 prime numbers
            int n5 = 2, x3;
            double sumOfPrimes = 2;

            while (n5 <=500)
            {
                for (x3 = 2; x3 < n5; x3++)
                {
                    if ((n5 % x) != 0)
                    {
                        sumOfPrimes = sumOfPrimes + n5;
                        x3 = n5 + 1;
                    }
                }
                n5++;
            }
            System.Console.WriteLine("Sum - " + sumOfPrimes);


        }

        public static bool result6(int n)
             {
                if (Math.Abs(n - 100) <= 20 || Math.Abs(n - 200) <= 20)
                    return true;
                return false;
            }
             public static int result5(int a, int b)
            {
                if (a > b)
                {
                    return (a - b) * 2;
                }
                return b - a;
             }   

    }
}



